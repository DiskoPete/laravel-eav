<?php

namespace DiskoPete\LaravelEav\Providers;

use DiskoPete\LaravelEav\Contracts\AttributeRepository as AttributeRepositoryContract;
use DiskoPete\LaravelEav\Contracts\EntityChecker as EntityCheckerContract;
use DiskoPete\LaravelEav\Contracts\Hydrator as HydratorContract;
use DiskoPete\LaravelEav\Contracts\ModelObserver as ModelObserverContract;
use DiskoPete\LaravelEav\Http\Middleware\LoadValuesMiddleware;
use DiskoPete\LaravelEav\Models\Attribute\Repository as AttributeRepository;
use DiskoPete\LaravelEav\Models\EntityChecker;
use DiskoPete\LaravelEav\Models\Hydrator;
use DiskoPete\LaravelEav\Models\Value\Repository as ValueRepository;
use DiskoPete\LaravelEav\Observers\ModelObserver;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Routing\Router;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function boot()
    {
        $this->registerMiddleware();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    private function registerMiddleware(): void
    {
        $router           = $this->getRouter();
        $middlewareGroups = $router->getMiddlewareGroups();

        foreach ($middlewareGroups as $key => $middlewareGroup) {

            $router->pushMiddlewareToGroup($key, LoadValuesMiddleware::class);
        }
    }

    private function getRouter(): Router
    {
        $this->initHttpKernel();
        return $this->app->make(Router::class);
    }

    private function initHttpKernel()
    {
        $this->app->make(Kernel::class);
    }

    public function register()
    {
        parent::register();

        $this->app->bind(
            ModelObserverContract::class,
            ModelObserver::class
        );

        $this->app->singleton(ValueRepository::class);
        $this->app->singleton(\DiskoPete\LaravelEav\Models\Value\Query\Factory::class);
        $this->app->singleton(\DiskoPete\LaravelEav\Models\Value\Aggregator\Factory::class);

        $this->app->singleton(AttributeRepositoryContract::class, AttributeRepository::class);
        $this->app->singleton(EntityCheckerContract::class, EntityChecker::class);
        $this->app->singleton(HydratorContract::class, Hydrator::class);
    }
}
