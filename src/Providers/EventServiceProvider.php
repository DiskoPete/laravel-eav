<?php

namespace DiskoPete\LaravelEav\Providers;

use DiskoPete\LaravelEav\Models\Attribute;
use DiskoPete\LaravelEav\Observers\AttributeObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    public function boot()
    {
        parent::boot();

        Attribute::observe(AttributeObserver::class);
    }
}
