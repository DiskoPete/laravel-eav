<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use DiskoPete\LaravelEav\Models\Attribute;

class CreateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Attribute::TABLE_NAME, function (Blueprint $table) {
            $table->increments(Attribute::COLUMN_ID);
            $table->string(Attribute::COLUMN_VALIDATION_RULES)->nullable();
            $table->string(Attribute::COLUMN_CODE);
            $table->string(Attribute::COLUMN_ENTITY_TYPE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes');
    }
}
