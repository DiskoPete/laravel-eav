<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use DiskoPete\LaravelEav\Models\Attribute;
use DiskoPete\LaravelEav\Models\Value;

class CreateValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Value::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger(Value::COLUMN_ATTRIBUTE_ID);
            $table->unsignedInteger(Value::COLUMN_ENTITY_ID);
            $table->text(Value::COLUMN_VALUE);
            $table->timestamps();

            $table->unique([
                Value::COLUMN_ATTRIBUTE_ID,
                Value::COLUMN_ENTITY_ID
            ]);

            $table
                ->foreign(Value::COLUMN_ATTRIBUTE_ID)
                ->references('id')
                ->on(Attribute::TABLE_NAME)
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('values');
    }
}
