<?php

namespace DiskoPete\LaravelEav\Http\Middleware;

use Closure;
use DiskoPete\LaravelEav\Contracts\EntityChecker;
use DiskoPete\LaravelEav\Contracts\Hydrator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class LoadValuesMiddleware
{
    /**
     * @var Hydrator
     */
    private $hydrator;
    /**
     * @var EntityChecker
     */
    private $entityChecker;

    public function __construct(
        Hydrator $hydrator,
        EntityChecker $entityChecker
    ) {
        $this->hydrator      = $hydrator;
        $this->entityChecker = $entityChecker;
    }

    public function handle(Request $request, Closure $next)
    {
        $params = $request->route()->parameters();

        foreach ($params as $param) {
            $this->processParam($param);
        }

        return $next($request);
    }

    private function processParam($param): void
    {
        if (!$this->canBeProcessed($param)) {
            return;
        }

        $this->hydrator->hydrate($param);
    }

    private function canBeProcessed($param): bool
    {
        return $param instanceof Model && $this->entityChecker->isEavEnabled($param);
    }
}
