<?php

namespace DiskoPete\LaravelEav\Listeners\Eloquent;

use DiskoPete\LaravelEav\Contracts\AttributeRepository;
use DiskoPete\LaravelEav\Contracts\EntityChecker;
use DiskoPete\LaravelEav\Models\Value;
use DiskoPete\LaravelEav\Models\Value\Repository as ValueRepository;
use Illuminate\Database\Eloquent\Model;


class Saved
{
    /**
     * @var ValueRepository
     */
    private $valueRepository;
    /**
     * @var Value\Aggregator
     */
    private $valueAggregatorFactory;
    /**
     * @var AttributeRepository
     */
    private $attributeRepository;
    /**
     * @var EntityChecker
     */
    private $entityChecker;

    public function __construct(
        AttributeRepository $attributeRepository,
        ValueRepository $valueRepository,
        Value\Aggregator\Factory $valueAggregatorFactory,
        EntityChecker $entityChecker
    )
    {
        $this->valueRepository        = $valueRepository;
        $this->valueAggregatorFactory = $valueAggregatorFactory;
        $this->attributeRepository    = $attributeRepository;
        $this->entityChecker          = $entityChecker;
    }

    public function handle(string $event, array $models): void
    {

        collect($models)
            ->each(function (Model $model) {
                $this->saveValues($model);
            });
    }


    public function saveValues(Model $model): void
    {

        $values = $model->getRelationValue(Saving::RELATION_KEY_VALUES);

        if (!$this->entityChecker->isEavEnabled($model) || !$values instanceof \Illuminate\Support\Collection) {
            return;
        }

        $values->each(function (Value $value) use ($model) {

            $attribute        = $this->attributeRepository->get($value->attribute_id);
            $value->entity_id = $model->getKey();

            if ($value->value === null) {
                $this->valueRepository->delete($value);
                return;
            }

            $this->valueRepository->upsert($value);

            $model->{$attribute->code} = $value->value;
        });

        $model->unsetRelation(Saving::RELATION_KEY_VALUES);
    }
}