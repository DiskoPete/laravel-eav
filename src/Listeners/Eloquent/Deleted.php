<?php

namespace DiskoPete\LaravelEav\Listeners\Eloquent;

use DiskoPete\LaravelEav\Contracts\EntityChecker;
use DiskoPete\LaravelEav\Models\Value;
use DiskoPete\LaravelEav\Models\Value\Query\Factory as ValueQueryFactory;
use Illuminate\Database\Eloquent\Model;

class Deleted
{

    /**
     * @var ValueQueryFactory
     */
    private $valueQueryFactory;
    /**
     * @var EntityChecker
     */
    private $entityChecker;

    public function __construct(
        ValueQueryFactory $valueQueryFactory,
        EntityChecker $entityChecker
    ) {
        $this->valueQueryFactory = $valueQueryFactory;
        $this->entityChecker     = $entityChecker;
    }

    public function handle(string $event, array $models): void
    {
        collect($models)
            ->each(function (Model $model) {

                if (!$this->entityChecker->isEavEnabled($model)) {
                    return;
                }

                $this->deleteValues($model);
            });
    }

    private function deleteValues(Model $model): void
    {
        $query = $this->valueQueryFactory->make();
        $query->addEntityFilter($model);

        $collection = $query->get();

        $collection
            ->each(function (Value $value) {
                $value->delete();
            });
    }
}
