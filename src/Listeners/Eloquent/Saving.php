<?php

namespace DiskoPete\LaravelEav\Listeners\Eloquent;


use DiskoPete\LaravelEav\Contracts\AttributeRepository;
use DiskoPete\LaravelEav\Contracts\EntityChecker;
use DiskoPete\LaravelEav\Models\Attribute;
use DiskoPete\LaravelEav\Models\Value;
use DiskoPete\LaravelEav\Models\Value\Repository as ValueRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Saving
{
    const RELATION_KEY_VALUES = 'transfer_values';

    private ValueRepository $valueRepository;

    private Value\Aggregator\Factory $valueAggregatorFactory;

    private AttributeRepository $attributeRepository;

    private EntityChecker $entityChecker;

    public function __construct(
        AttributeRepository $attributeRepository,
        ValueRepository $valueRepository,
        Value\Aggregator\Factory $valueAggregatorFactory,
        EntityChecker $entityChecker
    )
    {
        $this->valueRepository        = $valueRepository;
        $this->valueAggregatorFactory = $valueAggregatorFactory;
        $this->attributeRepository    = $attributeRepository;
        $this->entityChecker          = $entityChecker;
    }

    public function handle(string $event, array $models): void
    {
        collect($models)
            ->each(fn(Model $model) => $this->extractValues($model));
    }

    private function extractValues(Model $model): void
    {
        if (!$this->entityChecker->isEavEnabled($model)) {
            return;
        }
        $aggregator = $this->valueAggregatorFactory->make();
        $values     = $aggregator->aggregate($model);
        $attributes = $this->getAttributes()
            ->where(Attribute::COLUMN_ENTITY_TYPE, get_class($model));


        $attributes->each(function (Attribute $attribute) use ($model) {
            $model->offsetUnset($attribute->code);
        });

        $model->setRelation(self::RELATION_KEY_VALUES, $values);
    }

    private function getAttributes(): Collection
    {
        return $this->attributeRepository->getList();
    }
}