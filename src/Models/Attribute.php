<?php

namespace DiskoPete\LaravelEav\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string code
 * @property string entity_type
 * @property string|null validation_rules
 */
class Attribute extends Model
{
    public const COLUMN_ID               = 'id';
    public const COLUMN_CODE             = 'code';
    public const COLUMN_ENTITY_TYPE      = 'entity_type';
    public const COLUMN_VALIDATION_RULES = 'validation_rules';
    public const TABLE_NAME              = 'eav_attributes';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::COLUMN_ID;
}
