<?php

namespace DiskoPete\LaravelEav\Models\Entity;

use DiskoPete\LaravelEav\Contracts\AttributeRepository;
use DiskoPete\LaravelEav\Models\Attribute;
use DiskoPete\LaravelEav\Models\Value;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class Query extends Builder
{

    private Collection $joinedValues;

    public function where($column, $operator = null, $value = null, $boolean = 'and')
    {
        $column = $this->prefixColumn($column);

        return parent::where($column, $operator, $value, $boolean);
    }

    /**
     * @param string|array $code
     * @return Query
     */
    public function addAttributeToSelect($code): Query
    {
        if ($code == '*') {
            $code = $this->getAttributeRepository()
                ->getListByEntity($this->getModel())
                ->pluck(Attribute::COLUMN_CODE)
                ->toArray();
        }

        $code = Arr::wrap($code);

        collect($code)
            ->each(fn(string $code) => $this->joinValue($code));

        return $this;
    }

    private function joinValue(string $code): void
    {
        $attribute    = $this->getAttributeRepository()->getByCode($this->getModel(), $code);
        $tableAs      = 'value_' . $code;
        $tableAsValue = $tableAs . '.' . Value::COLUMN_VALUE;

        $this->leftJoin(
            Value::TABLE_NAME . ' as ' . $tableAs,
            function (JoinClause $join) use ($tableAsValue, $tableAs, $attribute) {

                $model = $this->getModel();

                $join->on(
                    $tableAs . '.' . Value::COLUMN_ENTITY_ID,
                    '=',
                    $model->getTable() . '.' . $model->getKeyName()
                );
                $join->where($tableAs . '.' . Value::COLUMN_ATTRIBUTE_ID, $attribute->getKey());
            }
        );

        $this->getJoinedValues()->push($tableAsValue . ' as ' . $code);
    }

    private function getJoinedValues(): Collection
    {
        if (!isset($this->joinedValues)) {
            $this->joinedValues = collect();
        }

        return $this->joinedValues;
    }

    public function addAttributeToFilter(string $code, $operator = null, $value = null, $boolean = 'and'): Query
    {
        $this->joinValue($code);
        $tableAs      = 'value_' . $code;
        $tableAsValue = $tableAs . '.' . Value::COLUMN_VALUE;

        $this->where($tableAsValue, $operator, $value, $boolean);

        return $this;
    }

    private function shouldAugmentColumns(): bool
    {
        return $this->getJoinedValues()->isNotEmpty();
    }

    public function get($columns = ['*'])
    {
        $columns = collect($columns);

        if ($this->shouldAugmentColumns()) {
            $columns = $columns
                ->map(fn(string $column) => $this->prefixColumn($column))
                ->merge($this->getJoinedValues());
        }

        if ($this->query->columns) {
            $this->augmentQueryColumns($columns);
        }

        return parent::get($columns->all());
    }

    private function augmentQueryColumns($columns): void
    {
        $this->getJoinedValues()
            ->merge($columns)
            ->map(fn(string $column) => $this->prefixColumn($column))
            ->unique()
            ->reject(fn(string $column) => in_array($column, $this->query->columns ?: []))
            ->each(fn(string $column) => $this->addSelect($column));
    }

    public function toSql()
    {
        if ($this->shouldAugmentColumns()) {
            $this->augmentQueryColumns('*');
        }

        return parent::toSql();
    }

    private function getAttributeRepository(): AttributeRepository
    {
        return app(AttributeRepository::class);
    }

    private function prefixColumn($column)
    {
        if (!is_string($column)) {
            return $column;
        }

        if (strpos($column, '.') !== false) {
            return $column;
        }

        return $this->getModel()->getTable() . '.' . $column;
    }
}
