<?php

namespace DiskoPete\LaravelEav\Models\Attribute;

use DiskoPete\LaravelEav\Contracts\AttributeRepository;
use DiskoPete\LaravelEav\Models\Attribute;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Repository implements AttributeRepository
{
    /**
     * @var Collection
     */
    private $attributes;

    public function getByCode($entityType, string $code): Attribute
    {
        if (is_object($entityType)) {
            $entityType = get_class($entityType);
        }

        $attribute = $this->getList()
            ->where(Attribute::COLUMN_CODE, $code)
            ->where(Attribute::COLUMN_ENTITY_TYPE, $entityType)
            ->first();

        if (!$attribute) {
            throw new ModelNotFoundException("Can't find attribute: {$code}");
        }

        return $attribute;
    }

    public function get(int $id): Attribute
    {
        $attribute = $this->getList()->find($id);

        if (!$attribute) {
            throw new ModelNotFoundException();
        }

        return $attribute;
    }

    public function getList(): Collection
    {
        if (!$this->attributes) {
            $this->attributes = Attribute::all();
        }

        return $this->attributes;
    }

    public function getListByEntity(Model $entity): Collection
    {
        return $this->getList()->where(Attribute::COLUMN_ENTITY_TYPE, get_class($entity));
    }

    public function flush(): void
    {
        $this->attributes = null;
    }
}
