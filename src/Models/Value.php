<?php

namespace DiskoPete\LaravelEav\Models;

use Illuminate\Database\Eloquent\Model;
use DiskoPete\LaravelEav\Models\Value\Query;

/**
 * @property string|null value
 * @property int|null entity_id
 * @property int|null attribute_id
 */
class Value extends Model
{
    public const COLUMN_ATTRIBUTE_ID = 'attribute_id';
    public const COLUMN_ENTITY_ID    = 'entity_id';
    public const COLUMN_VALUE        = 'value';
    public const TABLE_NAME          = 'eav_values';

    protected $table = self::TABLE_NAME;

    protected $fillable = [
        self::COLUMN_ATTRIBUTE_ID,
        self::COLUMN_ENTITY_ID,
        self::COLUMN_VALUE,
    ];

    public function newEloquentBuilder($query): Query
    {
        return new Query($query);
    }
}
