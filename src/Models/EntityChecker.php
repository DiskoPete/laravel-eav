<?php


namespace DiskoPete\LaravelEav\Models;


use DiskoPete\LaravelEav\Contracts\EntityChecker as EntityCheckerContract;
use DiskoPete\LaravelEav\Traits\EavEnabled;
use Illuminate\Database\Eloquent\Model;

class EntityChecker implements EntityCheckerContract
{

    public function isEavEnabled(Model $entity): bool
    {
        return in_array(
            EavEnabled::class,
            class_uses($entity)
        );
    }
}