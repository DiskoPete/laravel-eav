<?php


namespace DiskoPete\LaravelEav\Models;


use DiskoPete\LaravelEav\Contracts\AttributeRepository;
use DiskoPete\LaravelEav\Contracts\Hydrator as HydratorContract;
use DiskoPete\LaravelEav\Models\Value\Query\Factory;
use Illuminate\Database\Eloquent\Model;

class Hydrator implements HydratorContract
{
    private Factory $valueQueryFactory;

    private AttributeRepository $attributeRepository;

    public function __construct(
        Factory $valueQueryFactory,
        AttributeRepository $attributeRepository
    )
    {
        $this->valueQueryFactory   = $valueQueryFactory;
        $this->attributeRepository = $attributeRepository;
    }

    public function hydrate(Model $entity, array $only = []): void
    {
        $query           = $this->valueQueryFactory->make();
        $valueCollection = $query->addEntityFilter($entity);

        if ($only) {
            $this->applyAttributeFilter(
                $entity,
                $valueCollection,
                $only
            );
        }

        $valueCollection
            ->toBase()
            ->get()
            ->each(function (\stdClass $value) use ($entity) {

                $attribute = $this->attributeRepository->get($value->{Value::COLUMN_ATTRIBUTE_ID});

                $entity->{$attribute->code} = $value->{Value::COLUMN_VALUE};
            });
    }

    private function applyAttributeFilter(Model $entity, Value\Query $valueCollection, array $attributeCodes)
    {
        foreach ($attributeCodes as $attributeCode) {
            $valueCollection->addAttributeFilter(
                $this->attributeRepository->getByCode(
                    get_class($entity),
                    $attributeCode
                )
            );
        }
    }
}
