<?php

namespace DiskoPete\LaravelEav\Models\Value\Aggregator;

use Illuminate\Contracts\Container\Container;
use DiskoPete\LaravelEav\Models\Value\Aggregator;

class Factory
{
    /**
     * @var Container
     */
    private $container;

    public function __construct(
        Container $container
    ) {
        $this->container = $container;
    }

    public function make(): Aggregator
    {
        return $this->container->make(Aggregator::class);
    }
}
