<?php

namespace DiskoPete\LaravelEav\Models\Value\Query;

use DiskoPete\LaravelEav\Models\Value;
use DiskoPete\LaravelEav\Models\Value\Query;

class Factory
{

    /**
     * @var Value
     */
    private $model;

    public function __construct(Value $model)
    {

        $this->model = $model;
    }

    public function make(): Query
    {
        return $this->model->newQueryWithoutScopes();
    }
}
