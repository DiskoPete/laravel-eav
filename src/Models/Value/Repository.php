<?php

namespace DiskoPete\LaravelEav\Models\Value;

use DiskoPete\LaravelEav\Models\Value;

class Repository
{
    public function upsert(Value $value): void
    {
        Value::query()->updateOrInsert(
            [
                Value::COLUMN_ATTRIBUTE_ID => $value->attribute_id,
                Value::COLUMN_ENTITY_ID    => $value->entity_id
            ],
            [
                Value::COLUMN_VALUE => $value->value
            ]
        );
    }

    public function delete(Value $value): void
    {
        if ($value->getKey()) {
            $value->delete();
            return;
        }

        $collection = Value::query()
            ->where(Value::COLUMN_ENTITY_ID, $value->entity_id)
            ->where(Value::COLUMN_ATTRIBUTE_ID, $value->attribute_id);


        $collection->each(function (Value $value) {
            $value->delete();
        });
    }
}
