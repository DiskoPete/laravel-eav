<?php

namespace DiskoPete\LaravelEav\Models\Value;

use DiskoPete\LaravelEav\Models\Attribute;
use DiskoPete\LaravelEav\Models\Value;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;

class Query extends Builder
{

    public function addEntityFilter(Model $entity): Query
    {
        return $this
            ->where(Value::COLUMN_ENTITY_ID, $entity->getKey())
            ->join(
                Attribute::TABLE_NAME,
                function (JoinClause $join) use ($entity) {

                    $join->on(
                        Attribute::TABLE_NAME . '.' . Attribute::COLUMN_ID,
                        '=',
                        Value::TABLE_NAME . '.' . Value::COLUMN_ATTRIBUTE_ID
                    );

                    $join->where(Attribute::COLUMN_ENTITY_TYPE, get_class($entity));
                }
            )
            ->select(Value::TABLE_NAME . '.*');
    }

    public function addAttributeFilter(\DiskoPete\LaravelEav\Models\Attribute $attribute)
    {
        return $this->where(
            Value::COLUMN_ATTRIBUTE_ID,
            $attribute->getKey()
        );
    }
}
