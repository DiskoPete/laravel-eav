<?php

namespace DiskoPete\LaravelEav\Models\Value;

use DiskoPete\LaravelEav\Contracts\AttributeRepository;
use DiskoPete\LaravelEav\Models\Attribute;
use DiskoPete\LaravelEav\Models\Value;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;

class Aggregator
{
    private Collection $values;

    private AttributeRepository $attributeRepository;

    private ValidationFactory $validationFactory;

    public function __construct(
        ValidationFactory $validationFactory,
        AttributeRepository $attributeRepository
    )
    {
        $this->values              = collect();
        $this->attributeRepository = $attributeRepository;
        $this->validationFactory   = $validationFactory;
    }

    public function aggregate(Model $entity): Collection
    {
        $attributesData = $entity->attributesToArray();
        $attributes     = $this->attributeRepository->getListByEntity($entity);

        $this->validate($attributesData, $entity);

        foreach ($attributesData as $attributeCode => $value) {
            $attribute = $attributes
                ->where(Attribute::COLUMN_CODE, $attributeCode)
                ->first();

            if (!$attribute) {
                continue;
            }

            $this->values->push(
                new Value([
                    Value::COLUMN_ATTRIBUTE_ID => $attribute->getKey(),
                    Value::COLUMN_VALUE        => $value
                ])
            );
        }

        return $this->values;
    }

    private function validate(array $attributesData, Model $entity): void
    {

        $rules     = $this->collectValidationRules($entity);
        $validator = $this->validationFactory->make($attributesData, $rules);

        if (!$validator->fails()) {
            return;
        }

        throw new ValidationException($validator);
    }

    private function collectValidationRules(Model $entity): array
    {
        $currentAttributes = $entity->getAttributes();

        return $this->attributeRepository
            ->getListByEntity($entity)
            ->where(Attribute::COLUMN_VALIDATION_RULES, '!=', null)
            ->filter(fn(Attribute $attribute) => $entity->exists ? array_key_exists($attribute->code, $currentAttributes) : true)
            ->mapWithKeys(fn(Attribute $attribute) => [$attribute->code => $attribute->validation_rules])
            ->all();
    }
}
