<?php


namespace DiskoPete\LaravelEav\Contracts;


use DiskoPete\LaravelEav\Models\Attribute;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface AttributeRepository
{
    public function getByCode($entityType, string $code): Attribute;

    public function get(int $id): Attribute;

    public function getList(): Collection;

    public function getListByEntity(Model $entity): Collection;

    public function flush(): void;
}
