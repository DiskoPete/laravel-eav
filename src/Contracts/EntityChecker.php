<?php


namespace DiskoPete\LaravelEav\Contracts;


use Illuminate\Database\Eloquent\Model;

interface EntityChecker
{
    public function isEavEnabled(Model $entity): bool;
}