<?php


namespace DiskoPete\LaravelEav\Contracts;


use Illuminate\Database\Eloquent\Model;

interface Hydrator
{
    public function hydrate(Model $entity, array $only = []): void;
}
