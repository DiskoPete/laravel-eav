<?php


namespace DiskoPete\LaravelEav\Contracts;


use Illuminate\Database\Eloquent\Model;

interface ModelObserver
{
    public function saving(Model $model): void;

    public function saved(Model $model): void;

    public function deleted(Model $model): void;
}
