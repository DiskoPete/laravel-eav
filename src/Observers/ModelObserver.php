<?php


namespace DiskoPete\LaravelEav\Observers;


use DiskoPete\LaravelEav\Contracts\ModelObserver as ModelObserverContract;
use DiskoPete\LaravelEav\Listeners\Eloquent\Deleted;
use DiskoPete\LaravelEav\Listeners\Eloquent\Saved;
use DiskoPete\LaravelEav\Listeners\Eloquent\Saving;
use Illuminate\Database\Eloquent\Model;

class ModelObserver implements ModelObserverContract
{
    private Deleted $deletedHandler;

    private Saving $savingHandler;

    private Saved $savedHandler;

    public function __construct(
        Saving $savingHandler,
        Saved $savedHandler,
        Deleted $deletedHandler
    )
    {
        $this->deletedHandler = $deletedHandler;
        $this->savingHandler  = $savingHandler;
        $this->savedHandler   = $savedHandler;
    }

    public function saving(Model $model): void
    {
        $this->savingHandler->handle('saving', [$model]);
    }

    public function saved(Model $model): void
    {
        $this->savedHandler->handle('saved', [$model]);
    }

    public function deleted(Model $model): void
    {
        $this->deletedHandler->handle('deleted', [$model]);
    }
}