<?php


namespace DiskoPete\LaravelEav\Observers;


use DiskoPete\LaravelEav\Contracts\AttributeRepository;
use DiskoPete\LaravelEav\Models\Attribute;

class AttributeObserver
{
    /**
     * @var AttributeRepository
     */
    private $attributeRepository;

    public function __construct(AttributeRepository $attributeRepository)
    {
        $this->attributeRepository = $attributeRepository;
    }

    public function saved(Attribute $attribute): void
    {
        $this->flushRepository();
    }

    private function flushRepository(): void
    {
        $this->attributeRepository->flush();
    }

    public function deleted(Attribute $attribute): void
    {
        $this->flushRepository();
    }
}
