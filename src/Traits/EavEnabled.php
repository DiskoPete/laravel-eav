<?php


namespace DiskoPete\LaravelEav\Traits;


use DiskoPete\LaravelEav\Contracts\AttributeRepository;
use DiskoPete\LaravelEav\Contracts\ModelObserver;
use DiskoPete\LaravelEav\Models\Attribute;
use DiskoPete\LaravelEav\Models\Entity\Query;
use Illuminate\Support\Collection;

trait EavEnabled
{

    public static function bootEavEnabled(): void
    {
        static::observe(
            app(ModelObserver::class)
        );
    }

    public function initializeEavEnabled()
    {
        if (method_exists(get_parent_class($this), 'fill')) {
            return;
        }

        throw new \RuntimeException('Fill method is not existing');
    }

    public function fill(array $attributes)
    {

        if ($attributes) {
            $this->fillEav($attributes);
        }

        return parent::fill($attributes);
    }

    private function fillEav(array &$values): void
    {

        $this
            ->getEntityAttributes()
            ->each(function (Attribute $attribute) use (&$values) {

                if (!array_key_exists($attribute->code, $values)) {
                    return;
                }

                $this->{$attribute->code} = $values[$attribute->code];
                unset($values[$attribute->code]);
            });
    }

    private function getEntityAttributes(): Collection
    {
        return $this->getAttributeRepository()->getListByEntity($this);
    }

    private function getAttributeRepository(): AttributeRepository
    {
        return app(AttributeRepository::class);
    }

    protected function getQueryClassName(): string
    {
        return Query::class;
    }

    public function newEloquentBuilder($query): Query
    {
        $className = Query::class;

        if (method_exists($this, 'getQueryClassName')) {
            $className = $this->getQueryClassName();
        }

        return new $className($query);
    }

    public function newQueryForRestoration($ids)
    {
        /** @var Query $query */
        $query = parent::newQueryForRestoration($ids);

        $query->addAttributeToSelect('*');

        return $query;
    }
}