<?php

namespace DiskoPete\LaravelEav\Tests\Php\Unit\Value;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;
use DiskoPete\LaravelEav\Models\Attribute;
use DiskoPete\LaravelEav\Models\Value\Aggregator;
use DiskoPete\LaravelEav\Tests\Php\Utils\Car;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesAttributes;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesTestModels;
use DiskoPete\LaravelEav\Tests\Php\TestCase;

class AggregatorTest extends TestCase
{
    use RefreshDatabase;
    use CreatesAttributes;
    use CreatesTestModels;

    /**
     * @test
     */
    public function canAggregateValues(): void
    {
        $attribute            = $this->createAttribute();
        $otherEntityAttribute = $this->createAttribute(
            null,
            [
                Attribute::COLUMN_ENTITY_TYPE => Car::class
            ]
        );
        $aggregator           = $this->makeAggregator();
        $testModel            = $this->makeBook();

        $testModel->{$attribute->code}            = 'foobar';
        $testModel->{$otherEntityAttribute->code} = 'baz';

        $collection = $aggregator->aggregate($testModel);

        $this->assertInstanceOf(Collection::class, $collection);
        $this->assertCount(1, $collection);
        $this->assertEquals('foobar', $collection->first()->value);
    }

    private function makeAggregator(): Aggregator
    {
        return app(Aggregator\Factory::class)->make();
    }

    /**
     * @test
     */
    public function canThrowValidationException(): void
    {
        $attribute                             = $this->createAttribute();
        $requiredAttribute                     = $this->createAttribute('required');
        $aggregator                            = $this->makeAggregator();
        $testModel                             = $this->makeBook();
        $testModel->{$attribute->code}         = 'foobar';
        $testModel->{$requiredAttribute->code} = '';

        $this->expectException(ValidationException::class);

        $aggregator->aggregate($testModel);
    }

    /**
     * @test
     */
    public function canIgnoreNotExistingAttribute(): void
    {
        $aggregator = $this->makeAggregator();
        $testModel  = $this->makeBook();


        $testModel->foo = 'bar';

        $collection = $aggregator->aggregate($testModel);


        $this->assertInstanceOf(Collection::class, $collection);
        $this->assertCount(0, $collection);
    }
}
