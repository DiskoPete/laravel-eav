<?php

namespace DiskoPete\LaravelEav\Tests\Php\Unit\Value\Aggregator;

use DiskoPete\LaravelEav\Models\Value\Aggregator;
use DiskoPete\LaravelEav\Models\Value\Aggregator\Factory as AggregatorFactory;
use DiskoPete\LaravelEav\Tests\Php\TestCase;

class FactoryTest extends TestCase
{
    /**
     * @test
     */
    public function canMakeAggregatorInstance(): void
    {
        $factory = $this->getFactory();

        $this->assertInstanceOf(Aggregator::class, $factory->make());
    }

    private function getFactory(): AggregatorFactory
    {
        return $this->app->make(AggregatorFactory::class);
    }
}
