<?php


namespace DiskoPete\LaravelEav\Tests\Php\Unit\Models;


use DiskoPete\LaravelEav\Contracts\Hydrator;
use DiskoPete\LaravelEav\Models\Attribute;
use DiskoPete\LaravelEav\Models\Value;
use DiskoPete\LaravelEav\Tests\Php\TestCase;
use DiskoPete\LaravelEav\Tests\Php\Utils\Book;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesAttributes;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesTestModels;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesValues;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HydratorTest extends TestCase
{
    use RefreshDatabase;
    use CreatesTestModels;
    use CreatesAttributes;
    use CreatesValues;

    /**
     * @test
     */
    public function canHydrateEntities(): void
    {
        $attribute = $this->createAttribute(null, [Attribute::COLUMN_ENTITY_TYPE => Book::class]);
        $book      = $this->createBook();

        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $attribute->getKey(),
            Value::COLUMN_ENTITY_ID    => $book->getKey(),
            Value::COLUMN_VALUE        => 'foobar'
        ]);

        self::assertNull($book->getAttribute($attribute->code));

        $this->getHydrator()->hydrate($book);

        self::assertEquals('foobar', $book->getAttribute($attribute->code));
    }

    /**
     * @test
     */
    public function canHydrateOnlySpecifiedAttributes(): void
    {
        $attributeA = $this->createAttribute(null, [Attribute::COLUMN_ENTITY_TYPE => Book::class]);
        $attributeB = $this->createAttribute(null, [Attribute::COLUMN_ENTITY_TYPE => Book::class]);
        $book       = $this->createBook();

        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $attributeA->getKey(),
            Value::COLUMN_ENTITY_ID    => $book->getKey(),
            Value::COLUMN_VALUE        => 'foobar'
        ]);

        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $attributeB->getKey(),
            Value::COLUMN_ENTITY_ID    => $book->getKey(),
            Value::COLUMN_VALUE        => 'bazbar'
        ]);

        self::assertNull($book->getAttribute($attributeA->code));
        self::assertNull($book->getAttribute($attributeB->code));

        $this->getHydrator()->hydrate($book, [$attributeA->code]);

        self::assertEquals('foobar', $book->getAttribute($attributeA->code));
        self::assertNull($book->getAttribute($attributeB->code));
    }

    private function getHydrator(): Hydrator
    {
        return $this->app->make(Hydrator::class);
    }
}