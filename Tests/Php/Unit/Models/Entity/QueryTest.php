<?php

namespace DiskoPete\LaravelEav\Tests\Php\Unit\Models\Entity;


use DiskoPete\LaravelEav\Models\Entity\Query;
use DiskoPete\LaravelEav\Models\Value;
use DiskoPete\LaravelEav\Tests\Php\TestCase;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesAttributes;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesTestModels;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesValues;
use Illuminate\Foundation\Testing\RefreshDatabase;

class QueryTest extends TestCase
{
    use RefreshDatabase;
    use CreatesAttributes;
    use CreatesValues;
    use CreatesTestModels;

    /**
     * @test
     */
    public function canEagerLoadValue(): void
    {
        $attribute = $this->createAttribute();
        $book      = $this->createBook();
        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $attribute->getKey(),
            Value::COLUMN_ENTITY_ID    => $book->getKey(),
            Value::COLUMN_VALUE        => 'foobar'
        ]);
        $query = $this->makeQuery();

        $query->addAttributeToSelect($attribute->code);

        $collection = $query->get();
        $foundBook  = $collection->first();

        self::assertTrue($book->is($foundBook));
        self::assertEquals('foobar', $foundBook->{$attribute->code});
    }

    private function makeQuery(): Query
    {
        $book = $this->makeBook();

        return $book->newQueryWithoutScopes();
    }

    /**
     * @test
     */
    public function canEagerLoadAllValues(): void
    {
        $attributeA = $this->createAttribute();
        $attributeB = $this->createAttribute();
        $book       = $this->createBook();
        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $attributeA->getKey(),
            Value::COLUMN_ENTITY_ID    => $book->getKey(),
            Value::COLUMN_VALUE        => 'foobar'
        ]);
        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $attributeB->getKey(),
            Value::COLUMN_ENTITY_ID    => $book->getKey(),
            Value::COLUMN_VALUE        => 'bazbar'
        ]);
        $query = $this->makeQuery();

        $query->addAttributeToSelect('*');

        $collection = $query->get();
        $foundBook  = $collection->first();

        $this->assertTrue($book->is($foundBook));
        $this->assertEquals('foobar', $foundBook->{$attributeA->code});
        $this->assertEquals('bazbar', $foundBook->{$attributeB->code});
    }

    /**
     * @test
     */
    public function canJoinMultipleAttributeValues(): void
    {
        $attributeA = $this->createAttribute();
        $attributeB = $this->createAttribute();
        $book       = $this->createBook();
        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $attributeA->getKey(),
            Value::COLUMN_ENTITY_ID    => $book->getKey(),
            Value::COLUMN_VALUE        => 'foobar'
        ]);
        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $attributeB->getKey(),
            Value::COLUMN_ENTITY_ID    => $book->getKey(),
            Value::COLUMN_VALUE        => 'bazbar'
        ]);
        $query = $this->makeQuery();

        $query->addAttributeToSelect([$attributeA->code, $attributeB->code]);

        $collection = $query->get();
        $foundBook  = $collection->first();

        $this->assertTrue($book->is($foundBook));
        $this->assertEquals('foobar', $foundBook->{$attributeA->code});
        $this->assertEquals('bazbar', $foundBook->{$attributeB->code});
    }

    /**
     * @test
     */
    public function canFindByWhereId(): void
    {
        $attributeA = $this->createAttribute();
        $book       = $this->createBook();
        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $attributeA->getKey(),
            Value::COLUMN_ENTITY_ID    => $book->getKey(),
            Value::COLUMN_VALUE        => 'foobar'
        ]);

        $foundBook = $this->makeQuery()
            ->addAttributeToSelect('*')
            ->where('id', $book->getKey())
            ->firstOrFail();

        self::assertTrue($book->is($foundBook));
    }

    /**
     * @test
     */
    public function canBeUsedInSubQuery(): void
    {
        $attribute = $this->createAttribute();
        $book       = $this->createBook();
        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $attribute->getKey(),
            Value::COLUMN_ENTITY_ID    => $book->getKey(),
            Value::COLUMN_VALUE        => 'foobar'
        ]);

        $foundBook = $this->makeQuery()->from(
            $this->makeQuery()->addAttributeToSelect('*')
        )->firstOrFail();

        self::assertTrue($book->is($foundBook));
        self::assertEquals('foobar', $foundBook->getAttributeValue($attribute->code));
    }
}