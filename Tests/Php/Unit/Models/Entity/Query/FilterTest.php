<?php


namespace DiskoPete\LaravelEav\Tests\Php\Unit\Models\Entity\Query;


use DiskoPete\LaravelEav\Models\Attribute;
use DiskoPete\LaravelEav\Models\Entity\Query;
use DiskoPete\LaravelEav\Models\Value;
use DiskoPete\LaravelEav\Tests\Php\TestCase;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesAttributes;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesTestModels;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesValues;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FilterTest extends TestCase
{
    use RefreshDatabase;
    use CreatesAttributes;
    use CreatesValues;
    use CreatesTestModels;

    /**
     * @test
     */
    public function canFilterByAttributes(): void
    {
        $attribute = $this->createAttribute();
        $bookFoo   = $this->createBook();
        $bookBar   = $this->createBook();
        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $attribute->getKey(),
            Value::COLUMN_ENTITY_ID    => $bookFoo->getKey(),
            Value::COLUMN_VALUE        => 'foo'
        ]);
        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $attribute->getKey(),
            Value::COLUMN_ENTITY_ID    => $bookBar->getKey(),
            Value::COLUMN_VALUE        => 'bar'
        ]);

        $collection = $this->makeQuery()
            ->addAttributeToFilter($attribute->code, 'foo')
            ->get();

        self::assertEquals('foo', $collection->first()->getAttribute($attribute->code));

        self::assertTrue($collection->contains($bookFoo));
        self::assertFalse($collection->contains($bookBar));
    }

    private function makeQuery(): Query
    {
        $book = $this->makeBook();

        return $book->newQueryWithoutScopes();
    }

    /**
     * @test
     */
    public function canFilterByMultipleAttributes(): void
    {
        $author   = $this->createAttribute();
        $year     = $this->createAttribute();
        $book2019 = $this->createBook();
        $book2018 = $this->createBook();

        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $author->getKey(),
            Value::COLUMN_ENTITY_ID    => $book2019->getKey(),
            Value::COLUMN_VALUE        => 'foo'
        ]);
        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $year->getKey(),
            Value::COLUMN_ENTITY_ID    => $book2019->getKey(),
            Value::COLUMN_VALUE        => 2019
        ]);
        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $author->getKey(),
            Value::COLUMN_ENTITY_ID    => $book2018->getKey(),
            Value::COLUMN_VALUE        => 'foo'
        ]);
        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $year->getKey(),
            Value::COLUMN_ENTITY_ID    => $book2018->getKey(),
            Value::COLUMN_VALUE        => 2018
        ]);

        $collection = $this->makeQuery()
            ->addAttributeToFilter($author->code, 'foo')
            ->addAttributeToFilter($year->code, 2019)
            ->get();

        self::assertTrue($collection->contains($book2019));
        self::assertFalse($collection->contains($book2018));
    }

    /**
     * @test
     */
    public function canOrFilterByMultipleAttributes(): void
    {
        $author   = $this->createAttribute(null, [Attribute::COLUMN_CODE => 'author']);
        $year     = $this->createAttribute(null, [Attribute::COLUMN_CODE => 'year']);
        $book2019 = $this->createBook();
        $book2018 = $this->createBook();
        $book2017 = $this->createBook();

        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $author->getKey(),
            Value::COLUMN_ENTITY_ID    => $book2019->getKey(),
            Value::COLUMN_VALUE        => 'foo'
        ]);

        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $year->getKey(),
            Value::COLUMN_ENTITY_ID    => $book2018->getKey(),
            Value::COLUMN_VALUE        => 2018
        ]);

        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $year->getKey(),
            Value::COLUMN_ENTITY_ID    => $book2017->getKey(),
            Value::COLUMN_VALUE        => 2017
        ]);

        $collection = $this->makeQuery()
            ->addAttributeToFilter($author->code, 'foo', null, 'or')
            ->addAttributeToFilter($year->code, 2018, null, 'or')
            ->get();

        self::assertTrue($collection->contains($book2019));
        self::assertTrue($collection->contains($book2018));
        self::assertFalse($collection->contains($book2017));
    }
}