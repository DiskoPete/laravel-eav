<?php

namespace DiskoPete\LaravelEav\Tests\Php\Unit\Models\Value;

use DiskoPete\LaravelEav\Models\Value;
use DiskoPete\LaravelEav\Models\Value\Repository;
use DiskoPete\LaravelEav\Tests\Php\TestCase;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesAttributes;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesTestModels;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesValues;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RepositoryTest extends TestCase
{
    use RefreshDatabase;
    use CreatesTestModels;
    use CreatesAttributes;
    use CreatesValues;

    /**
     * @test
     */
    public function canInsertValue(): void
    {
        $attribute  = $this->createAttribute();
        $event      = $this->createBook();
        $repository = $this->getRepository();

        $value = new Value([
            Value::COLUMN_ATTRIBUTE_ID => $attribute->getKey(),
            Value::COLUMN_ENTITY_ID    => $event->getKey(),
            Value::COLUMN_VALUE        => 'foobar'
        ]);

        $repository->upsert($value);

        $this->assertDatabaseHas(
            Value::TABLE_NAME,
            [
                Value::COLUMN_ATTRIBUTE_ID => $attribute->getKey(),
                Value::COLUMN_ENTITY_ID    => $event->getKey(),
                Value::COLUMN_VALUE        => 'foobar'
            ]
        );
    }

    private function getRepository(): Repository
    {
        return app(Repository::class);
    }

    /**
     * @test
     */
    public function canUpdateValue(): void
    {
        $attribute  = $this->createAttribute();
        $event      = $this->createBook();
        $value      = $this->createValue(null, [
            Value::COLUMN_ENTITY_ID    => $event->getKey(),
            Value::COLUMN_ATTRIBUTE_ID => $attribute->getKey(),
        ]);
        $repository = $this->getRepository();


        $this->assertDatabaseMissing(
            Value::TABLE_NAME,
            [
                Value::COLUMN_ATTRIBUTE_ID => $attribute->getKey(),
                Value::COLUMN_ENTITY_ID    => $event->getKey(),
                Value::COLUMN_VALUE        => 'foobar'
            ]
        );

        $value->value = 'foobar';

        $repository->upsert($value);


        $this->assertDatabaseHas(
            Value::TABLE_NAME,
            [
                Value::COLUMN_ATTRIBUTE_ID => $attribute->getKey(),
                Value::COLUMN_ENTITY_ID    => $event->getKey(),
                Value::COLUMN_VALUE        => 'foobar'
            ]
        );
    }

    /**
     * @test
     */
    public function canDeleteValue(): void
    {
        $value      = $this->createValue();
        $repository = $this->getRepository();


        $this->assertDatabaseHas(
            Value::TABLE_NAME,
            [
                'id' => $value->getKey(),
            ]
        );

        $repository->delete($value);

        $this->assertDatabaseMissing(
            Value::TABLE_NAME,
            [
                'id' => $value->getKey(),
            ]
        );

        $value      = $this->createValue();
        $newValue = new Value();
        $newValue->entity_id = $value->entity_id;
        $newValue->attribute_id = $value->attribute_id;

        $this->assertDatabaseHas(
            Value::TABLE_NAME,
            [
                'id' => $value->getKey(),
            ]
        );

        $repository->delete($value);

        $this->assertDatabaseMissing(
            Value::TABLE_NAME,
            [
                'id' => $value->getKey(),
            ]
        );
    }
}
