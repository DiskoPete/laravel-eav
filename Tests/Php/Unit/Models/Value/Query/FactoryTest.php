<?php

namespace DiskoPete\LaravelEav\Tests\Php\Unit\Models\Value\Query;

use DiskoPete\LaravelEav\Models\Value\Query;
use DiskoPete\LaravelEav\Models\Value\Query\Factory;
use DiskoPete\LaravelEav\Tests\Php\TestCase;

class FactoryTest extends TestCase
{

    private function getFactory(): Factory
    {
        return $this->app->make(Factory::class);
    }

    /**
     * @test
     */
    public function canMakeQuery(): void
    {
        $factory = $this->getFactory();
        $this->assertInstanceOf(Query::class, $factory->make());
    }
}
