<?php

namespace DiskoPete\LaravelEav\Tests\Php\Unit\Models\Value;

use Illuminate\Foundation\Testing\RefreshDatabase;
use DiskoPete\LaravelEav\Models\Attribute;
use DiskoPete\LaravelEav\Models\Value;
use DiskoPete\LaravelEav\Models\Value\Query;
use DiskoPete\LaravelEav\Models\Value\Query\Factory;
use DiskoPete\LaravelEav\Tests\Php\Utils\Car;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesAttributes;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesTestModels;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesValues;
use DiskoPete\LaravelEav\Tests\Php\TestCase;

class QueryTest extends TestCase
{
    use RefreshDatabase;
    use CreatesAttributes;
    use CreatesValues;
    use CreatesTestModels;

    /**
     * @test
     */
    public function canFilterByEntity(): void
    {
        $anotherAttribute = $this->createAttribute(null, [
            Attribute::COLUMN_ENTITY_TYPE => Car::class
        ]);
        $entity           = $this->createBook();
        $value            = $this->createValue(null, [
            Value::COLUMN_ENTITY_ID => $entity->getKey(),
        ]);
        $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $anotherAttribute->getKey()
        ]);
        $query = $this->makeQuery();

        $query->addEntityFilter($entity);

        $collection = $query->get();

        $this->assertCount(1, $collection);
        $this->assertTrue($value->is($collection->first()));
    }

    private function makeQuery(): Query
    {
        return $this->app->make(Factory::class)->make();
    }

    /**
     * @test
     */
    public function canFilterByAttribute(): void
    {
        $attribute = $this->createAttribute();
        $value     = $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $attribute->getKey(),
        ]);
        $this->createValue();

        $query = $this->makeQuery();

        $query->addAttributeFilter($attribute);

        $collection = $query->get();

        $this->assertCount(1, $collection);
        $this->assertTrue($value->is($collection->first()));
    }
}
