<?php

namespace DiskoPete\LaravelEav\Tests\Php\Unit\Attribute;

use DiskoPete\LaravelEav\Contracts\AttributeRepository;
use DiskoPete\LaravelEav\Models\Attribute;
use DiskoPete\LaravelEav\Tests\Php\TestCase;
use DiskoPete\LaravelEav\Tests\Php\Utils\Book;
use DiskoPete\LaravelEav\Tests\Php\Utils\Car;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesAttributes;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesTestModels;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery\MockInterface;

class RepositoryTest extends TestCase
{

    use RefreshDatabase;
    use CreatesAttributes;
    use CreatesTestModels;

    /**
     * @var AttributeRepository
     */
    private $attributeRepository;

    /**
     * @test
     */
    public function canGetByCode()
    {
        $attribute      = $this->createAttribute();
        $foundAttribute = $this->attributeRepository->getByCode($attribute->entity_type, $attribute->code);

        $this->assertTrue($foundAttribute->is($attribute));
    }

    /**
     * @test
     */
    public function canGetById(): void
    {
        $attribute      = $this->createAttribute();
        $foundAttribute = $this->attributeRepository->get($attribute->getKey());

        $this->assertTrue($foundAttribute->is($attribute));
    }

    /**
     * @test
     */
    public function canGetByEntityType(): void
    {
        $attribute        = $this->createAttribute();
        $this->createAttribute(null, [
            Attribute::COLUMN_ENTITY_TYPE => Car::class
        ]);

        $collection = $this->attributeRepository->getListByEntity($this->createBook());

        $this->assertCount(1, $collection);
        $this->assertTrue($attribute->is($collection->first()));
    }

    /**
     * @test
     */
    public function canGetByIdThrowsException(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->attributeRepository->get(99);
    }

    /**
     * @test
     */
    public function throwsException()
    {
        $this->expectException(ModelNotFoundException::class);
        $this->attributeRepository->getByCode(Book::class, 'foobar');
    }

    /**
     * @test
     */
    public function isFlushedWhenAttributeChanges(): void
    {
        $this->mock(AttributeRepository::class, function (MockInterface $repository) {
            $repository->shouldReceive('flush')->times(3);
        });

        $attribute = $this->createAttribute();

        $attribute->code = 'foobar';
        $attribute->save();

        $attribute->delete();

    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->attributeRepository = $this->app->make(AttributeRepository::class);
    }
}
