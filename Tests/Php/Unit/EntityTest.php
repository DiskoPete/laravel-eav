<?php

namespace DiskoPete\LaravelEav\Tests\Php\Unit;

use DiskoPete\LaravelEav\Models\Value;
use DiskoPete\LaravelEav\Tests\Php\TestCase;
use DiskoPete\LaravelEav\Tests\Php\Utils\Book;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesAttributes;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesTestModels;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesValues;
use DiskoPete\LaravelEav\Tests\Php\Utils\Serializable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Queue\SerializesModels;
use Illuminate\Validation\ValidationException;

class EntityTest extends TestCase
{

    use RefreshDatabase;
    use CreatesTestModels;
    use CreatesAttributes;
    use CreatesValues;

    /**
     * @test
     */
    public function canSaveAttributeValuesForNewModel(): void
    {

        $attribute = $this->createAttribute();
        $book      = $this->makeBook();

        $book->setAttribute($attribute->code, 'foobar');

        $book->save();

        $this->assertDatabaseHas(Value::TABLE_NAME, [
            Value::COLUMN_ENTITY_ID => $book->getKey(),
            Value::COLUMN_VALUE     => 'foobar'
        ]);

        $this->assertEquals('foobar', $book->{$attribute->code});
    }

    /**
     * @test
     */
    public function canSaveAttributeValuesForExistingModel(): void
    {

        $attribute = $this->createAttribute();
        $model     = $this->createBook();

        $model->setAttribute($attribute->code, 'foobar');

        $model->save();

        $this->assertDatabaseHas(Value::TABLE_NAME, [
            Value::COLUMN_ENTITY_ID => $model->getKey(),
            Value::COLUMN_VALUE     => 'foobar'
        ]);
    }

    /**
     * @test
     */
    public function doesNotSaveNullValues(): void
    {

        $attribute = $this->createAttribute();
        $model     = $this->createBook();

        $model->setAttribute($attribute->code, null);

        $model->save();

        $this->assertDatabaseMissing(Value::TABLE_NAME, [
            Value::COLUMN_ENTITY_ID => $model->getKey(),
            Value::COLUMN_ENTITY_ID => $attribute->getKey()
        ]);
    }

    /**
     * @test
     */
    public function cantSaveAnothersEntityAttribute(): void
    {

        $bookAttribute = $this->createAttribute();
        $carAttribute  = $this->createAttribute('car');
        $book          = $this->makeBook();

        $this->expectException(\PDOException::class);

        $book->setAttribute($bookAttribute->code, 'foobar');
        $book->setAttribute($carAttribute->code, 'foobar');

        $book->save();

        $this->assertDatabaseHas(Value::TABLE_NAME, [
            Value::COLUMN_ENTITY_ID    => $book->getKey(),
            Value::COLUMN_ATTRIBUTE_ID => $bookAttribute->code,
            Value::COLUMN_VALUE        => 'foobar'
        ]);

        $this->assertDatabaseMissing(Value::TABLE_NAME, [
            Value::COLUMN_ENTITY_ID    => $book->getKey(),
            Value::COLUMN_ATTRIBUTE_ID => $carAttribute->code,
            Value::COLUMN_VALUE        => 'foobar'
        ]);

    }

    /**
     * @test
     */
    public function validatesValues(): void
    {
        $attribute = $this->createAttribute('required');
        $model     = $this->makeBook();

        $model->setAttribute($attribute->code, '');

        $this->expectException(ValidationException::class);
        $model->save();
    }

    /**
     * @test
     */
    public function canUpdateEntityWithPartialData(): void
    {
        $requiredAttribute = $this->createAttribute('required');
        $otherAttribute = $this->createAttribute();
        $book     = $this->makeBook();
        $saveErrors = 0;

        $book->setAttribute($requiredAttribute->code, 'foo');

        self::assertTrue($book->save());

        $book = Book::query()->find($book->getKey());
        $book->setAttribute($otherAttribute->code, 'baz');
        self::assertTrue($book->save());


        try {
            $book->setAttribute($requiredAttribute->code, null);
            $book->save();
        } catch (ValidationException $e) {
            $saveErrors++;
        }

        try {
            $book->setAttribute($requiredAttribute->code, '');
            $book->save();
        } catch (ValidationException $e) {
            $saveErrors++;
        }

        self::assertEquals(2, $saveErrors);
    }

    /**
     * @test
     */
    public function deletesItsValuesAfterDeleting(): void
    {

        $attribute = $this->createAttribute();
        $model     = $this->createBook();
        $value     = $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $attribute->getKey(),
            Value::COLUMN_ENTITY_ID    => $model->getKey()
        ]);

        $this->assertDatabaseHas(Value::TABLE_NAME, [
            'id' => $value->getKey()
        ]);

        $model->delete();


        $this->assertDatabaseMissing(Value::TABLE_NAME, [
            'id' => $value->getKey()
        ]);
    }

    /**
     * @test
     */
    public function canUpdateExistingAttributeValue(): void
    {

        $attribute = $this->createAttribute();
        $value     = $this->createValue(null, [
            Value::COLUMN_ATTRIBUTE_ID => $attribute->getKey()
        ]);
        $model     = $this->createBook();

        $this->assertNotEquals('foobar', $value->value);
        $model->setAttribute($attribute->code, 'foobar');

        $model->save();

        $this->assertDatabaseHas(Value::TABLE_NAME, [
            Value::COLUMN_ENTITY_ID => $model->getKey(),
            Value::COLUMN_VALUE     => 'foobar'
        ]);
    }

    /**
     * @test
     */
    public function canFillEavValues(): void
    {

        $attribute = $this->createAttribute();
        $model     = $this->createBook();

        $model->fill([
            $attribute->code => 'foobar'
        ]);

        $this->assertEquals('foobar', $model->{$attribute->code});
    }

    /**
     * @test
     */
    public function restoresValuesOfSerializedModel(): void
    {
        $attribute = $this->createAttribute();
        $model     = $this->createBook([
            $attribute->code => 'foobar'
        ]);

        self::assertEquals('foobar', $model->{$attribute->code});


        $serializable = new Serializable($model);

        $result = serialize($serializable);
        $result = unserialize($result);

        self::assertEquals('foobar', $result->model->{$attribute->code});
    }
}
