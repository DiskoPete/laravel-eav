<?php

namespace DiskoPete\LaravelEav\Tests\Php\Utils;

use DiskoPete\LaravelEav\Models\Entity\Query;
use DiskoPete\LaravelEav\Traits\EavEnabled;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use EavEnabled;

    public const TABLE_NAME = 'books';

    protected $table = self::TABLE_NAME;
}
