<?php

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use DiskoPete\LaravelEav\Tests\Php\Utils\Book;

class CreateBookTestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!$this->isTesting()) {
            return;
        }

        Schema::create(Book::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');

            $table->timestamps();
        });
    }

    private function isTesting(): bool
    {
        return $this->getApp()->runningUnitTests();
    }

    private function getApp(): Application
    {
        return app(Application::class);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!$this->isTesting()) {
            return;
        }

        Schema::dropIfExists(Book::TABLE_NAME);
    }
}
