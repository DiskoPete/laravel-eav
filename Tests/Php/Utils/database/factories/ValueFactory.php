<?php

use DiskoPete\LaravelEav\Tests\Php\Utils\Book;
use Faker\Generator as Faker;
use DiskoPete\LaravelEav\Models\Attribute;
use DiskoPete\LaravelEav\Models\Value;

$factory->define(Value::class, function (Faker $faker) {
    return [
        Value::COLUMN_ATTRIBUTE_ID => function () {
            return factory(Attribute::class)->create()->getKey();
        },
        Value::COLUMN_ENTITY_ID    => function () {
            return factory(Book::class)->create()->getKey();
        },
        Value::COLUMN_VALUE        => $faker->word,
    ];
});
