<?php
/**
 * @var \Illuminate\Database\Eloquent\Factory $factory
 */

use DiskoPete\LaravelEav\Models\Attribute;
use DiskoPete\LaravelEav\Tests\Php\Utils\Book;
use DiskoPete\LaravelEav\Tests\Php\Utils\Car;
use Faker\Generator as Faker;

$factory->define(Attribute::class, function (Faker $faker) {
    return [
        Attribute::COLUMN_CODE        => str_replace('-', '_', $faker->slug(2)),
        Attribute::COLUMN_ENTITY_TYPE => Book::class
    ];
});

$factory->state(Attribute::class, 'required', function () {
    return [
        Attribute::COLUMN_VALIDATION_RULES => 'required'
    ];
});

$factory->state(Attribute::class, 'car', function () {
    return [
        Attribute::COLUMN_ENTITY_TYPE => Car::class
    ];
});
