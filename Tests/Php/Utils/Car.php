<?php

namespace DiskoPete\LaravelEav\Tests\Php\Utils;

use DiskoPete\LaravelEav\Traits\EavEnabled;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use EavEnabled;
    
    public const TABLE_NAME = 'car';

    protected $table = self::TABLE_NAME;
}
