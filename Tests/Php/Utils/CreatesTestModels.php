<?php

namespace DiskoPete\LaravelEav\Tests\Php\Utils;

trait CreatesTestModels
{
    private function makeBook(): Book
    {
        return factory(Book::class)->make();
    }

    private function createBook(array $attributes = []): Book
    {
        return factory(Book::class)->create($attributes);
    }
}
