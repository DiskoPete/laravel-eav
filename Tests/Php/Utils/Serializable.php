<?php


namespace DiskoPete\LaravelEav\Tests\Php\Utils;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;

class Serializable
{
    use SerializesModels;

    /**
     * @var Model
     */
    public $model;

    public function __construct(
        Model $model
    )
    {
        $this->model = $model;
    }


}