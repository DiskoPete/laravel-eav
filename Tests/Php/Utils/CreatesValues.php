<?php

namespace DiskoPete\LaravelEav\Tests\Php\Utils;

use DiskoPete\LaravelEav\Models\Value;

trait CreatesValues
{
    private function createValue($states = null, array $attributes = []): Value
    {

        $factoryBuilder = factory(Value::class);


        if ($states) {
            $factoryBuilder->states($states);
        }

        return $factoryBuilder->create($attributes);
    }
}
