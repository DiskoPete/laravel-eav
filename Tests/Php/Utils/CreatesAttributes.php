<?php

namespace DiskoPete\LaravelEav\Tests\Php\Utils;

use DiskoPete\LaravelEav\Models\Attribute;

trait CreatesAttributes
{
    private function createAttribute($states = null, array $attributes = []): Attribute
    {

        $factoryBuilder = factory(Attribute::class);


        if ($states) {
            $factoryBuilder->states($states);
        }

        return $factoryBuilder->create($attributes);
    }
}
