<?php

namespace DiskoPete\LaravelEav\Tests\Php\Feature\Middleware;

use DiskoPete\LaravelEav\Contracts\Hydrator;
use DiskoPete\LaravelEav\Tests\Php\TestCase;
use DiskoPete\LaravelEav\Tests\Php\Utils\Book;
use DiskoPete\LaravelEav\Tests\Php\Utils\CreatesTestModels;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Route;
use Mockery\MockInterface;

class LoadValuesTest extends TestCase
{

    use RefreshDatabase;
    use CreatesTestModels;

    /**
     * @test
     */
    public function shouldCallHydrateMethod(): void
    {
        $this->mock(Hydrator::class, function (MockInterface $hydrator) {
            $hydrator->shouldReceive('hydrate')->once();
        });

        $book = $this->createBook();

        $this->get(route('test', $book));

    }

    protected function setUp(): void
    {
        parent::setUp();

        Route::group(
            [
                'prefix'     => 'test',
                'middleware' => 'web',
            ],
            function () {
                Route::get(
                    '{model}',
                    function (Book $model) {
                        return $model;
                    }
                )->name('test');
            }
        );

    }
}
